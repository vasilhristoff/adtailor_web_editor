var gulp = require('gulp');
var sass = require('gulp-sass');

function handleError(error) {
    console.log(error.toString());
    this.emit('end')
}

gulp.task('sass', function () {
    return gulp.src('./public/sass/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./public/css'));
})

gulp.task('watch', function () {
    gulp.watch('./public/sass/style.scss', ['sass'])
        .on('error', handleError)
})