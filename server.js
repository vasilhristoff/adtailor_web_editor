var express = require('express');
var engine = require('ejs-locals');
var http = require('http');

var app = express();

app.engine('ejs', engine);
app.set('views', __dirname+'/views');
app.use(express.static(__dirname+'/public'));

var port = 3000;

app.get('/' , function (req, res) {
    res.send('Empty string')  
})

app.get('/v1', function () {
    res.send('')
})

var server = http.createServer(app);
server.listen(port, function () {
    console.log('app is running on port ' + port );    
})